package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter
{
    //some code

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter an integer value and press enter.");
        int x = scan.nextInt();

        Utilities u = new Utilities();
        int doubled = u.doubleMe(x);
        System.out.println("It double is: "+ doubled);
    }
}